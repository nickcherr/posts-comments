package main

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

var postType = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Post",
	Description: "A Post",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.Int),
			Description: "The identifier of the post.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if post, ok := p.Source.(*Post); ok {
					return post.ID, nil
				}

				return nil, nil
			},
		},
		"content": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "The content of the post.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if post, ok := p.Source.(*Post); ok {
					return post.Content, nil
				}

				return nil, nil
			},
		},
		"allow_comments": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.Boolean),
			Description: "Is it possible to comment on the post.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if post, ok := p.Source.(*Post); ok {
					return post.AllowComments, nil
				}

				return nil, nil
			},
		},
	},
})

var commentType = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Comment",
	Description: "A Comment",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.Int),
			Description: "The identifier of the comment.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if comment, ok := p.Source.(*Comment); ok {
					return comment.ID, nil
				}

				return nil, nil
			},
		},
		"post_id": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.Int),
			Description: "The identifier of the post that owns this comment.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if comment, ok := p.Source.(*Comment); ok {
					return comment.PostID, nil
				}

				return nil, nil
			},
		},
		"depth": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.Int),
			Description: "The depth of the comment.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if comment, ok := p.Source.(*Comment); ok {
					return comment.Depth, nil
				}

				return nil, nil
			},
		},
		"full_path": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "The full path to the comment.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if comment, ok := p.Source.(*Comment); ok {
					return comment.FullPath, nil
				}

				return nil, nil
			},
		},
		"content": &graphql.Field{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "The content of the comment.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if comment, ok := p.Source.(*Comment); ok {
					return comment.Content, nil
				}

				return nil, nil
			},
		},
	},
})

var rootQuery = graphql.NewObject(graphql.ObjectConfig{
	Name: "RootQuery",
	Fields: graphql.Fields{
		"posts": &graphql.Field{
			Type:        graphql.NewList(postType),
			Description: "List of all posts.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if *inMemory {
					fmt.Println(postsData)
					return postsData, nil
				}

				rows, err := db.Query(`
						SELECT id, content, allow_comments 
						FROM posts_comments.posts`)
				if err != nil {
					panic(err)
				}
				var posts []*Post

				for rows.Next() {
					post := &Post{}

					err = rows.Scan(&post.ID, &post.Content, &post.AllowComments)
					if err != nil {
						panic(err)
					}
					posts = append(posts, post)
				}

				return posts, nil
			},
		},
		"post": &graphql.Field{
			Type:        postType,
			Description: "Get a post by ID.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, _ := params.Args["id"].(int)

				if *inMemory {
					if len(postsData) < id {
						return nil, nil
					}
					return postsData[id-1], nil
				}

				post := &Post{}
				err := db.QueryRow(`
						SELECT id, content, allow_comments 
						FROM posts_comments.posts 
						WHERE id = $1`,
					id).Scan(&post.ID, &post.Content, &post.AllowComments)
				if err != nil {
					panic(err)
				}

				return post, nil
			},
		},
		"comments": &graphql.Field{ //?
			Type:        graphql.NewList(commentType),
			Description: "List of all comments.",
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if *inMemory {
					return commentsData, nil
				}

				rows, err := db.Query(`
						SELECT id, post_id, depth, full_path, content 
						FROM posts_comments.comments`)
				if err != nil {
					panic(err)
				}
				var comments []*Comment

				for rows.Next() {
					comment := &Comment{}

					err = rows.Scan(&comment.ID, &comment.PostID, &comment.Depth, &comment.FullPath, &comment.Content)
					if err != nil {
						panic(err)
					}
					comments = append(comments, comment)
				}

				return comments, nil
			},
		},
		"commentsToPost": &graphql.Field{
			Type:        graphql.NewList(commentType),
			Description: "Get comments to a post by post ID.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				id, _ := p.Args["id"].(int)

				if *inMemory {
					res := []*Comment{}
					for _, comment := range commentsData {
						if comment.PostID == id {
							res = append(res, comment)
						}
					}
					return res, nil
				}

				rows, err := db.Query(`
						SELECT id, post_id, depth, full_path, content 
						FROM posts_comments.comments 
						WHERE post_id = $1`,
					id)
				if err != nil {
					panic(err)
				}
				var comments []*Comment

				for rows.Next() {
					comment := &Comment{}

					err = rows.Scan(&comment.ID, &comment.PostID, &comment.Depth, &comment.FullPath, &comment.Content)
					if err != nil {
						panic(err)
					}
					comments = append(comments, comment)
				}

				return comments, nil
			},
		},
		"commentsToPostPaginated": &graphql.Field{
			Type:        graphql.NewList(commentType),
			Description: "Get comments to a post by post ID with pagination.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"start": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"end": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				id, _ := p.Args["id"].(int)
				start, _ := p.Args["start"].(int)
				end, _ := p.Args["end"].(int)

				if *inMemory {
					amounts := map[string]int{}
					res := []*Comment{}

					for _, comment := range commentsData {
						if amounts[comment.FullPath] >= start && amounts[comment.FullPath] < end {
							res = append(res, comment)
						}
						amounts[comment.FullPath]++
					}

					return res, nil
				}

				rows, err := db.Query(`
						SELECT id, post_id, depth, full_path, content
						FROM (
							SELECT id, post_id, depth, full_path, content,
							ROW_NUMBER () OVER (
								PARTITION BY full_path
								ORDER BY full_path, id
							) local_rank
							FROM posts_comments.comments c
							WHERE post_id = $1
						) subselect
						WHERE local_rank > $2 AND local_rank <= $3`,
					id, start, end)
				if err != nil {
					panic(err)
				}
				var comments []*Comment

				for rows.Next() {
					comment := &Comment{}

					err = rows.Scan(&comment.ID, &comment.PostID, &comment.Depth, &comment.FullPath, &comment.Content)
					if err != nil {
						panic(err)
					}
					comments = append(comments, comment)
				}

				return comments, nil
			},
		},
		"commentsToCommentPaginated": &graphql.Field{
			Type:        graphql.NewList(commentType),
			Description: "Get extra comments to a comment with pagination.",
			Args: graphql.FieldConfigArgument{
				"full_path": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"start": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
				"end": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				start, _ := p.Args["start"].(int)
				end, _ := p.Args["end"].(int)
				full_path, _ := p.Args["full_path"].(string)

				if *inMemory {
					amount := 0
					res := []*Comment{}

					for _, comment := range commentsData {
						if comment.FullPath != full_path {
							continue
						}
						if amount >= start && amount < end {
							res = append(res, comment)
						}
						amount++
					}

					return res, nil
				}

				rows, err := db.Query(`
						SELECT id, post_id, depth, full_path, content
						FROM (
							SELECT id, post_id, depth, full_path, content,
							ROW_NUMBER () OVER (
								PARTITION BY full_path
								ORDER BY full_path, id
							) local_rank
							FROM posts_comments.comments c
							WHERE full_path = $1
						) subselect
						WHERE local_rank > $2 AND local_rank <= $3`,
					full_path, start, end)
				if err != nil {
					panic(err)
				}
				var comments []*Comment

				for rows.Next() {
					comment := &Comment{}

					err = rows.Scan(&comment.ID, &comment.PostID, &comment.Depth, &comment.FullPath, &comment.Content)
					if err != nil {
						panic(err)
					}
					comments = append(comments, comment)
				}

				return comments, nil
			},
		},
		"comment": &graphql.Field{
			Type:        commentType,
			Description: "Get a comment by ID.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, _ := params.Args["id"].(int)

				if *inMemory {
					if len(commentsData) < id {
						return nil, nil
					}
					return commentsData[id-1], nil
				}

				comment := &Comment{}
				err := db.QueryRow(`
						SELECT id, post_id, depth, full_path, content 
						FROM posts_comments.comments WHERE id = $1`,
					id).Scan(&comment.ID, &comment.PostID, &comment.Depth, &comment.FullPath, &comment.Content)
				if err != nil {
					panic(err)
				}

				return comment, nil
			},
		},
	},
})

var rootMutation = graphql.NewObject(graphql.ObjectConfig{
	Name: "RootMutation",
	Fields: graphql.Fields{
		"createPost": &graphql.Field{
			Type:        postType,
			Description: "Create new post",
			Args: graphql.FieldConfigArgument{
				"content": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"allow_comments": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Boolean),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				content, _ := params.Args["content"].(string)
				allowComments, _ := params.Args["allow_comments"].(bool)

				if *inMemory {
					newPost := &Post{
						ID:            len(postsData) + 1,
						Content:       content,
						AllowComments: allowComments,
					}
					postsData = append(postsData, newPost)
					return newPost, nil
				}

				var lastInsertId int
				err := db.QueryRow(`
						INSERT INTO posts_comments.posts(content, allow_comments) 
						VALUES($1, $2) returning id;`,
					content, allowComments).Scan(&lastInsertId)
				if err != nil {
					panic(err)
				}

				newPost := &Post{
					ID:            lastInsertId,
					Content:       content,
					AllowComments: allowComments,
				}

				return newPost, nil
			},
		},
		"createComment": &graphql.Field{
			Type:        commentType,
			Description: "Create new comment",
			Args: graphql.FieldConfigArgument{
				"post_id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int),
				},
				"depth": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.Int), // ?
				},
				"full_path": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
				"content": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				postID, _ := params.Args["post_id"].(int)
				depth, _ := params.Args["depth"].(int)
				content, _ := params.Args["content"].(string)
				fullPath, _ := params.Args["full_path"].(string)

				if *inMemory {
					newComment := &Comment{
						ID:       len(commentsData) + 1,
						PostID:   postID,
						Depth:    depth,
						Content:  content,
						FullPath: fullPath,
					}
					commentsData = append(commentsData, newComment)
					return newComment, nil
				}

				var lastInsertId int
				err := db.QueryRow(`
						INSERT INTO posts_comments.comments(post_id, depth, full_path, content) 
						VALUES($1, $2, $3, $4) returning id;`,
					postID, depth, fullPath, content).Scan(&lastInsertId)
				if err != nil {
					panic(err)
				}

				newComment := &Comment{
					ID:       lastInsertId,
					PostID:   postID,
					Depth:    depth,
					FullPath: fullPath,
					Content:  content,
				}

				return newComment, nil
			},
		},
	},
})
