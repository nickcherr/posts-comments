package main

import "html/template"

type HrefTmplVars struct {
	Link  string
	Text  string
	Depth int
}

type PostTmplVars struct {
	ID            int
	ContentLines  []string
	AllowComments bool
}

type CommentTmplVars struct {
	Depth        int
	ContentLines []string
	PostID       int
	CommentID    int
}

var postMainPageTmpl = template.Must(template.New("page").Parse(`
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Пост на главной странице</title>
	</head>
	<body>
		<div>
			{{range .ContentLines}}
				<p>{{. | html}}</p>
			{{end}}
			<p><a href="/posts/{{.ID}}">Открыть пост</a>.</p>
			<p>------------------------------------</p>
			<br>
		</div>
	</body>
	</html>
`))

var postTmpl = template.Must(template.New("page").Parse(`
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Пост</title>
	</head>
	<body>
		<div>
			{{range .ContentLines}}
				<p>{{. | html}}</p>
			{{end}}
			<p>Комментарии разрешены: {{.AllowComments}}</p>
			<br>
		</div>
	</body>
	</html>
`))

var commentTmpl = template.Must(template.New("page").Parse(`
	<!DOCTYPE html>
	<form style="margin-left: calc({{.Depth}}*80px);">
	<html lang="en">
	<head>
		<style>
			.text-container {
				max-width: 600px;
				word-wrap: break-word;
			}
			div {
				max-width: 600px;
				min-width: 100px;
				background-color: powderblue;
			}
		</style>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Комментарий</title>
	</head>
	<body>
		<div>
			{{range .ContentLines}}
				<p>{{. | html}}</p>
			{{end}}
			<p><a href="/posts/{{.PostID}}/add_comment/{{.CommentID}}">Ответить</a></p>
			<br>
		</div>
	</body>
	</html>
	</form>
`))

var hrefTmpl = template.Must(template.New("page").Parse(`
	<!DOCTYPE html>
	<form style="margin-left: calc({{.Depth}}*80px);">
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Абсолютная ссылка</title>
	</head>
	<body>
		<p><a href="{{.Link}}">{{.Text}}</a></p>
		<br>
		<br>
	</body>
	</form>
	</html>
`))

var newPostFormTmpl = []byte(`
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>Форма для создания нового поста</title>
		</head>
		<body>
			<form method="POST">
				<label for="comment">Пост:</label><br>
				<textarea id="comment" name="comment" rows="4" cols="50"></textarea><br><br>

				<label for="allow-comment">Разрешить комментировать пост?</label><br>
				<input type="radio" id="yes" name="allow-comment" value="yes">
				<label for="yes">Да</label><br>
				<input type="radio" id="no" name="allow-comment" value="no">
				<label for="no">Нет</label><br><br>

				<input type="submit" value="Отправить">
			</form>
		</body>
		</html>
	`)

var newCommentFormTmpl = []byte(`
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>Форма для создания нового комментария</title>
		</head>
		<body>
			<form method="POST">
				<label for="comment">Комментарий:</label><br>
				<textarea id="comment" name="comment" rows="4" cols="50"></textarea><br><br>

				<input type="submit" value="Отправить">
			</form>
		</body>
		</html>
	`)
