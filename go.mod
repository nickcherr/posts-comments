module module

go 1.21.3

require (
	github.com/gorilla/mux v1.8.1
	github.com/graphql-go/graphql v0.8.1
	github.com/lib/pq v1.10.9
)
