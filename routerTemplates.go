package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/graphql-go/graphql"
)

func redirectToPostsRouting(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/posts", http.StatusSeeOther)
}

var (
	errUnmarshallJSON  = errors.New("can't unmarshal JSON")
	errAtoiConversion  = errors.New("can't converse String to Integer")
	errExecuteTemplate = errors.New("can't execute template")
)

func addPostRouting(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Write(newPostFormTmpl)
		return
	}

	content := r.FormValue("comment")
	var allowComments bool
	switch r.FormValue("allow-comment") {
	case "yes":
		allowComments = true
	case "no":
		allowComments = false
	}

	vars := map[string]interface{}{"content": content, "allow_comments": allowComments}

	params := graphql.Params{Schema: schema, VariableValues: vars, RequestString: createPostQuery}
	res := graphql.Do(params)
	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unmarshalPostMap map[string]map[string]Post
	json.Unmarshal(resJSON, &unmarshalPostMap)
	newPost := unmarshalPostMap["data"]["createPost"]
	newPostID := newPost.ID

	http.Redirect(w, r, "/posts/"+strconv.Itoa(newPostID), http.StatusSeeOther)
}

func postsRouting(w http.ResponseWriter, r *http.Request) {
	paginationInfo = map[string]int{}
	cachedComments = []Comment{}
	paginationPostID = 0

	hrefTmplVars := HrefTmplVars{
		Link: "/posts/new",
		Text: "Создать новый пост",
	}
	err := hrefTmpl.Execute(w, hrefTmplVars)
	if err != nil {
		log.Fatal(errExecuteTemplate)
	}

	params := graphql.Params{Schema: schema, RequestString: getPostsQuery}
	res := graphql.Do(params)
	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unm map[string]map[string][]Post
	json.Unmarshal(resJSON, &unm)
	posts := unm["data"]["posts"]

	for _, post := range posts {
		postTmplVars := PostTmplVars{
			ID:           post.ID,
			ContentLines: strings.Split(post.Content, "\n"),
		}
		err := postMainPageTmpl.Execute(w, postTmplVars)
		if err != nil {
			log.Fatal(errExecuteTemplate)
		}
	}
}

func postRouting(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ID := vars["id"]
	postID, err := strconv.Atoi(ID)
	if err != nil {
		log.Fatal(errAtoiConversion)
	}
	if paginationPostID != postID {
		paginationInfo = map[string]int{}
		cachedComments = []Comment{}
		paginationPostID = postID
	}
	hrefTmplVars := HrefTmplVars{
		Link: "/posts",
		Text: "Вернуться к списку постов",
	}
	err = hrefTmpl.Execute(w, hrefTmplVars)
	if err != nil {
		log.Fatal(errExecuteTemplate)
	}

	queryVars := map[string]interface{}{"id": postID}
	params := graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: getPostQuery}
	res := graphql.Do(params)
	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unm map[string]map[string]Post
	json.Unmarshal(resJSON, &unm)
	post := unm["data"]["post"]
	postTmplVars := PostTmplVars{
		AllowComments: post.AllowComments,
		ContentLines:  strings.Split(post.Content, "\n"),
	}
	err = postTmpl.Execute(w, postTmplVars)
	if err != nil {
		log.Fatal(errExecuteTemplate)
	}

	if !post.AllowComments {
		return
	}

	hrefTmplVars = HrefTmplVars{
		Link: "/posts/" + ID + "/add_comment",
		Text: "Добавить новый комментарий",
	}
	err = hrefTmpl.Execute(w, hrefTmplVars)
	if err != nil {
		log.Fatal(errExecuteTemplate)
	}

	paginationPortion := 5
	queryVars = map[string]interface{}{"id": postID, "start": paginationInfo[""], "end": paginationInfo[""] + paginationPortion}
	params = graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: getPaginatedCommentsToPost}
	res = graphql.Do(params)
	resJSON, err = json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unmarshalCommentsMap map[string]map[string][]Comment
	json.Unmarshal(resJSON, &unmarshalCommentsMap)
	comments := unmarshalCommentsMap["data"]["commentsToPostPaginated"]
	for i := range comments {
		paginationInfo[comments[i].FullPath]++
		comments[i].FullPath += fmt.Sprint(comments[i].ID) + "."
	}

	cachedComments = append(cachedComments, comments...)

	sort.Slice(cachedComments, func(i, j int) bool {
		pI, pJ := strings.Split(cachedComments[i].FullPath, "."), strings.Split(cachedComments[j].FullPath, ".")
		if pI[len(pI)-1] == "" {
			pI = pI[:len(pI)-1]
		}
		if pJ[len(pJ)-1] == "" {
			pJ = pJ[:len(pJ)-1]
		}
		k := 0
		for k < len(pI) && k < len(pJ) {
			pIk, err := strconv.Atoi(pI[k])
			if err != nil {
				log.Fatal(errAtoiConversion)
			}
			pJk, err := strconv.Atoi(pJ[k])
			if err != nil {
				log.Fatal(errAtoiConversion)
			}
			if pIk < pJk {
				return true
			} else if pIk > pJk {
				return false
			}
			k++
		}
		return k == len(pI)
	})

	for i, comment := range cachedComments {
		commentTmplVars := CommentTmplVars{
			Depth:        comment.Depth,
			PostID:       comment.PostID,
			CommentID:    comment.ID,
			ContentLines: strings.Split(comment.Content, "\n"),
		}
		err = commentTmpl.Execute(w, commentTmplVars)
		if err != nil {
			log.Fatal(errExecuteTemplate)
		}

		if i == len(cachedComments)-1 || comment.Depth > cachedComments[i+1].Depth {
			hrefTmplVars = HrefTmplVars{
				Link:  "/posts/" + ID + "/show_more/" + fmt.Sprint(comment.ID),
				Text:  "Показать больше комментариев",
				Depth: comment.Depth,
			}
			err = hrefTmpl.Execute(w, hrefTmplVars)
			if err != nil {
				log.Fatal(errExecuteTemplate)
			}
		}
	}

	hrefTmplVars = HrefTmplVars{
		Link: "/posts/" + ID,
		Text: "Показать больше комментариев",
	}
	err = hrefTmpl.Execute(w, hrefTmplVars)
	if err != nil {
		log.Fatal(errExecuteTemplate)
	}
}

func showMoreCommentsRouting(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ID := vars["id"]
	postID, err := strconv.Atoi(ID)
	if err != nil {
		log.Fatal(errAtoiConversion)
	}
	ID = vars["comment_id"]
	commentID, err := strconv.Atoi(ID)
	if err != nil {
		log.Fatal(errAtoiConversion)
	}

	if paginationPostID != postID {
		paginationInfo = map[string]int{}
		cachedComments = []Comment{}
		paginationPostID = postID
	}

	queryVars := map[string]interface{}{"id": commentID}
	params := graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: getCommentQuery}
	res := graphql.Do(params)
	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unmarshalCommentMap map[string]map[string]Comment
	json.Unmarshal(resJSON, &unmarshalCommentMap)
	comment := unmarshalCommentMap["data"]["comment"]

	paginationPortion := 5
	queryVars = map[string]interface{}{"full_path": comment.FullPath + fmt.Sprint(comment.ID) + ".", "start": paginationInfo[comment.FullPath], "end": paginationInfo[comment.FullPath] + paginationPortion}
	params = graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: getPaginatedCommentsToComment}
	res = graphql.Do(params)
	resJSON, err = json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unmarshalCommentsMap map[string]map[string][]Comment
	json.Unmarshal(resJSON, &unmarshalCommentsMap)
	comments := unmarshalCommentsMap["data"]["commentsToCommentPaginated"]
	for i := range comments {
		paginationInfo[comments[i].FullPath]++
		comments[i].FullPath += fmt.Sprint(comments[i].ID) + "."
	}

	cachedComments = append(cachedComments, comments...)

	http.Redirect(w, r, "/posts/"+fmt.Sprint(postID), http.StatusSeeOther)
}

func addRootCommentRouting(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Write(newCommentFormTmpl)
		return
	}
	vars := mux.Vars(r)
	ID := vars["id"]
	postID, err := strconv.Atoi(ID)
	if err != nil {
		log.Fatal(errAtoiConversion)
	}

	content := r.FormValue("comment")

	queryVars := map[string]interface{}{"content": content, "post_id": postID}
	params := graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: createRootCommentQuery}
	graphql.Do(params)
	http.Redirect(w, r, "/posts/"+strconv.Itoa(postID), http.StatusSeeOther)
}

func addCommentRouting(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Write(newCommentFormTmpl)
		return
	}

	vars := mux.Vars(r)
	ID := vars["id"]
	postID, err := strconv.Atoi(ID)
	if err != nil {
		log.Fatal(errAtoiConversion)
	}
	ID = vars["comment_id"]
	commentID, err := strconv.Atoi(ID)
	if err != nil {
		log.Fatal(errAtoiConversion)
	}

	content := r.FormValue("comment")

	queryVars := map[string]interface{}{"id": commentID}
	params := graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: getCommentQuery}
	res := graphql.Do(params)
	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Fatal(errUnmarshallJSON)
	}
	var unmarshalCommentMap map[string]map[string]Comment
	json.Unmarshal(resJSON, &unmarshalCommentMap)
	parentComment := unmarshalCommentMap["data"]["comment"]

	queryVars = map[string]interface{}{"content": content, "post_id": postID, "full_path": parentComment.FullPath + fmt.Sprint(commentID) + ".", "depth": parentComment.Depth + 1}
	params = graphql.Params{Schema: schema, VariableValues: queryVars, RequestString: createCommentQuery}
	http.Redirect(w, r, "/posts/"+strconv.Itoa(postID), http.StatusSeeOther)
	graphql.Do(params)
}
