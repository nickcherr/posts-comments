package main

var createRootCommentQuery = `
	mutation($post_id: Int!, $content: String!) {
		createComment(post_id: $post_id, depth: 0, full_path: "", content: $content) {
			id
		}
	}
`

var createCommentQuery = `
	mutation($post_id: Int!, $depth: Int!, $full_path: String!, $content: String!) {
		createComment(post_id: $post_id, depth: $depth, full_path: $full_path, content: $content) {
			id
		}
	}
`

var getCommentQuery = `
	query($id: Int!) {
		comment(id: $id) {
			post_id
			depth
			full_path
		}
	}
`

var getPaginatedCommentsToPost = `
	query($id: Int!, $start: Int!, $end: Int!) {
		commentsToPostPaginated(id: $id, start: $start, end: $end) {
			id
			depth
			full_path
			content
			post_id
		}
	}
`

var getPaginatedCommentsToComment = `
	query($full_path: String!, $start: Int!, $end: Int!) {
		commentsToCommentPaginated(full_path: $full_path, start: $start, end: $end) {
			id
			depth
			full_path
			content
			post_id
		}
	}
`

var getPostQuery = `
	query($id: Int!) {
		post(id: $id) {
			content
			allow_comments
		}
	}
`

var getPostsQuery = `
	query {
		posts {
			id
			content
		}
	}
`

var createPostQuery = `
	mutation($content: String!, $allow_comments: Boolean!) {
		createPost(content: $content, allow_comments: $allow_comments) {
			id
		}
	}
`
