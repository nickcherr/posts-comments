package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/graphql-go/graphql"
	_ "github.com/lib/pq"
)

const (
	DB_HOST     = "host.docker.internal"
	DB_PORT     = "5432"
	DB_USER     = "postgres"
	DB_PASSWORD = "postgres"
	DB_NAME     = "postgres"
)

type Post struct {
	ID            int    `json:"id"`
	Content       string `json:"content"`
	AllowComments bool   `json:"allow_comments"`
}

type Comment struct {
	ID       int    `json:"id"`
	PostID   int    `json:"post_id"`
	Depth    int    `json:"depth"`
	FullPath string `json:"full_path"`
	Content  string `json:"content"`
}

var dbinfo = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
	DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME)

var db *sql.DB
var postsData []*Post
var commentsData []*Comment
var inMemory *bool
var schema, err = graphql.NewSchema(graphql.SchemaConfig{
	Query:    rootQuery,
	Mutation: rootMutation,
})

var paginationInfo map[string]int
var paginationPostID int
var cachedComments []Comment

func main() {
	if err != nil {
		panic("Can't open PostgreSQL database")
	}
	inMemory = flag.Bool("inMemory", false, "In memory if true, PostgreSQL if false")
	flag.Parse()

	if !*inMemory {
		db, err = sql.Open("postgres", dbinfo)
		if err != nil {
			panic("Can't open PostgreSQL database")
		}

		defer db.Close()
	}

	router := mux.NewRouter()

	router.HandleFunc("/", redirectToPostsRouting)
	router.HandleFunc("", redirectToPostsRouting)
	router.HandleFunc("/posts", postsRouting)
	router.HandleFunc("/posts/new", addPostRouting)
	router.HandleFunc("/posts/{id}", postRouting)
	router.HandleFunc("/posts/{id}/add_comment", addRootCommentRouting)
	router.HandleFunc("/posts/{id}/add_comment/{comment_id}", addCommentRouting)
	router.HandleFunc("/posts/{id}/show_more/{comment_id}", showMoreCommentsRouting)

	fmt.Println("starting server at :8080")
	http.ListenAndServe(":8080", router)
}
