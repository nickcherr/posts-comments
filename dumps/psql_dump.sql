-- DROP TABLE posts_comments.posts;
-- DROP TABLE posts_comments.comments;
-- DROP SCHEMA posts_comments;

CREATE SCHEMA IF NOT EXISTS posts_comments;

CREATE TABLE IF NOT EXISTS posts_comments.posts (
    id SERIAL PRIMARY KEY,
    content TEXT NOT NULL,
    allow_comments BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS posts_comments.comments (
    id SERIAL PRIMARY KEY,
    post_id INT NOT NULL,
    depth INT NOT NULL,
    -- full_path INT[], -- NOT NULL,
    full_path TEXT, -- NOT NULL,
    content VARCHAR(2000) NOT NULL
);
